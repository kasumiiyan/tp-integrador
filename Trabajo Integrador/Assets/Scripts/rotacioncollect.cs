using UnityEngine;

public class rotacioncollect : MonoBehaviour
{
    public float rotationSpeed = 30f;

    void Update()
    {
        // Hace que el objeto rote lentamente en torno al eje Y.
        transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);
    }
}