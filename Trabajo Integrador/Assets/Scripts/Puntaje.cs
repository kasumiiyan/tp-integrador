using UnityEngine;
using UnityEngine.UI;

public class Puntaje : MonoBehaviour
{
    public int score = 0;
    public Text scoreText; // Asigna un objeto Text desde el Inspector para mostrar el puntaje en pantalla.
    public Text timerText; // Asigna otro objeto Text para mostrar el temporizador en pantalla.
    public Text winMessage;
    public Text loseMessage;

    public float timer = 60f; // Establece el tiempo inicial en segundos.
    private bool gameIsOver = false;

    void Update()
    {
        if (!gameIsOver)
        {
            // Actualiza el temporizador.
            timer -= Time.deltaTime;

            // Verifica si el tiempo ha llegado a cero.
            if (timer <= 0f)
            {
                LoseGame();
            }

            // Actualiza la UI.
            UpdateScoreUI();
        }
    }

    public void AddScore(int points)
    {
        if (!gameIsOver)
        {
            score += points;
            UpdateScoreUI();

            if (score >= 100)
            {
                WinGame();
            }
        }
    }

    void UpdateScoreUI()
    {
        // Actualiza el texto en pantalla con el puntaje actual.
        if (scoreText != null)
        {
            scoreText.text = score.ToString();
        }

        // Actualiza el texto en pantalla con el temporizador.
        if (timerText != null)
        {
            timerText.text = "Time: " + Mathf.Round(timer).ToString();
        }
    }

    void WinGame()
    {
        gameIsOver = true;
        winMessage.text = "Ganaste Meow!";
        Time.timeScale = 0f; // Pausa el juego.
    }

    void LoseGame()
    {
        gameIsOver = true;
        loseMessage.text = "Game Over!";
        Time.timeScale = 0f; // Pausa el juego.
    }
}