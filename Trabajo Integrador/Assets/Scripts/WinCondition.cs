using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinCondition : MonoBehaviour
{

    public Text textoGanaste;
    

    void Start()
    {

    }


    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {

            textoGanaste.text = "You Won Meow!";

            Camera mainCamera = Camera.main;

            // Verifica si la c�mara principal tiene un AudioSource
            if (mainCamera != null)
            {
                AudioSource backgroundMusic = mainCamera.GetComponent<AudioSource>();

                // Detiene la m�sica de fondo si se encuentra el AudioSource
                if (backgroundMusic != null)
                {
                    backgroundMusic.Stop();
                }
            }

            Time.timeScale = 0;
          
        }
    }
}
