using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoladePelo : MonoBehaviour
{
    public int damage = 10;
    public float tiempoVida;
    private void OnCollisionEnter(Collision collision)
    {
        // Verifica si el objeto con el que colision� tiene un componente de vida
        HealthScript healthScript = collision.gameObject.GetComponent<HealthScript>();

        if (healthScript != null)
        {
            // Si el objeto tiene un componente de vida, aplica da�o
            healthScript.TakeDamage(damage);
        }
        Destroy(gameObject, tiempoVida);
    
    }
}