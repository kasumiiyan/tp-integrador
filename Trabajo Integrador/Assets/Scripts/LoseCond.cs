using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoseCond : MonoBehaviour
{

    public Text textoPerder;


    void Start()
    {

    }


    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {

            textoPerder.text = "You Lose :( !";

            Camera mainCamera = Camera.main;

            // Verifica si la c�mara principal tiene un AudioSource
            if (mainCamera != null)
            {
                AudioSource backgroundMusic = mainCamera.GetComponent<AudioSource>();

                // Detiene la m�sica de fondo si se encuentra el AudioSource
                if (backgroundMusic != null)
                {
                    backgroundMusic.Stop();
                }
            }

            Time.timeScale = 0;

        }
    }
}
