using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hookdetector : MonoBehaviour
{

    public GameObject player;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Hookable")
        {
            player.GetComponent<grapplinghook>().hooked = true;
            player.GetComponent<grapplinghook>().hookedObj = other.gameObject;
        }
    }
}
