using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlTorreta : MonoBehaviour
{

    Transform Player;

    float dist;
    public float maxDistance;
    public Transform head, posShoot;
    public GameObject proyectil;
    public float shootSpeed;
    public float fireRate, nextFire;


    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player").transform;

    }

    // Update is called once per frame
    void Update()
    {
        dist = Vector3.Distance(Player.position, transform.position);
        if (dist <= maxDistance)
        {
            head.LookAt(Player);
            if (Time.time >= nextFire)
            {

                nextFire = Time.time + 1f / fireRate;
                Shoot();
            }
        }
    }

    void Shoot()
    {
        GameObject clone = Instantiate(proyectil, posShoot.position, head.rotation);
        clone.GetComponent<Rigidbody>().AddForce(head.forward * shootSpeed);
        Destroy(clone, 5);

    }
}