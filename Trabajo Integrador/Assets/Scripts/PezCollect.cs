using UnityEngine;

public class PezCollect : MonoBehaviour
{
    public int scoreValue = 20;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Collect(other.gameObject);
        }
    }

    void Collect(GameObject player)
    {
        Destroy(gameObject);


        Puntaje playerScore = player.GetComponent<Puntaje>();


        if (playerScore == null)
        {
            Puntaje[] playerScores = GameObject.FindObjectsOfType<Puntaje>();
            if (playerScores.Length > 0)
            {
                playerScore = playerScores[0];
            }
        }

        // Si se encuentra un componente PlayerScore, suma puntos.
        if (playerScore != null)
        {
            playerScore.AddScore(scoreValue);
        }
    }
}