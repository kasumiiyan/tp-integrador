using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidoSalto : MonoBehaviour
{
    private AudioSource audioSource;

    private void Start()
    {
        // Obt�n la referencia al componente AudioSource
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        // Detecta cuando el jugador salta 
        if (Input.GetButtonDown("Jump"))
        {
            // Reproduce el sonido
            PlayJumpSound();
        }
    }

    private void PlayJumpSound()
    {
        // Aseg�rate de que el AudioSource y el AudioClip est�n configurados
        if (audioSource != null && audioSource.clip != null)
        {
            // Reproduce el sonido
            audioSource.PlayOneShot(audioSource.clip);
        }
    }
}