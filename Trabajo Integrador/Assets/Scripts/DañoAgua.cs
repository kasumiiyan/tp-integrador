using UnityEngine;

public class DañoAgua : MonoBehaviour
{
    public int damagePerSecond = 1; // Cantidad de daño por segundo

    void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Acciones que se realizan cuando el jugador está encima de este objeto
            PlayerMovement player = other.GetComponent<PlayerMovement>();

            if (player != null)
            {
                // Aplica el daño al jugador cada segundo
                player.TakeDamage(damagePerSecond * Time.deltaTime);
            }

         
        }
    }
}
