using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blink : MonoBehaviour
{

    public float distance = 5.0f;
    private float playerYPos;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            BlinkForward();
        }
    }


    public void BlinkForward()
    {
        RaycastHit hit;
        Vector3 destination = transform.position + transform.forward * distance;
        playerYPos = transform.position.y;
        // si se detecta obstaculo que interfiere (evitar tpearse dentro de paredes) 
        if (Physics.Linecast(transform.position, destination, out hit))
        {
            destination = transform.position + transform.forward * (hit.distance - 1f);
        }

        // no se encuentra el obstáculo
        if (Physics.Raycast(destination, -Vector3.up, out hit))
        {
            destination = hit.point;
            destination.y = playerYPos;
            transform.position = destination;

        }
            
       
    }




}
