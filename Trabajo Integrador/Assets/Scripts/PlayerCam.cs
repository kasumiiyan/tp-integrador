using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCam : MonoBehaviour
{
    public float sensX;
    public float sensY;
    public Transform player; // Referencia al objeto del personaje
    float xRotation;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        // Obtiene la entrada del mouse
        float mouseX = Input.GetAxisRaw("Mouse X") * Time.deltaTime * sensX;
        float mouseY = Input.GetAxisRaw("Mouse Y") * Time.deltaTime * sensY;

        // Actualiza las rotaciones
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        // Rota la c�mara y orientaci�n
        transform.rotation = Quaternion.Euler(xRotation, player.rotation.eulerAngles.y, 0);

        // Aplica rotaci�n al personaje (puedes descomentar la siguiente l�nea si quieres rotar tambi�n al personaje)
        player.rotation *= Quaternion.Euler(0, mouseX, 0);

        // Aplica rotaci�n solo a la c�mara
        player.rotation *= Quaternion.Euler(0, mouseX, 0);

        // Evita que el jugador pueda mirar hacia arriba o abajo m�s all� de ciertos l�mites
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
    }
}