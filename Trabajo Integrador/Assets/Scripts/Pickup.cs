using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public LayerMask pickupLayer; // Capa de los objetos que se pueden recoger
    public float pickupRange = 5f; // Rango de alcance para recoger objetos
    public Transform pickupPoint; // Punto desde el cual recoger los objetos

    private Rigidbody currentObject;

    void Update()
    {
        if (Input.GetMouseButtonDown(1)) 
        {
            if (currentObject)
            {
                ReleaseObject();
            }
            else
            {
                PickupObject();
            }
        }
    }

    void FixedUpdate()
    {
        if (currentObject)
        {
            MoveObject();
        }
    }

    void PickupObject()
    {
        Ray pickupRay = new Ray(pickupPoint.position, pickupPoint.forward);

        if (Physics.Raycast(pickupRay, out RaycastHit hitInfo, pickupRange, pickupLayer))
        {
            currentObject = hitInfo.rigidbody;
            currentObject.useGravity = false;
        }
    }

    void ReleaseObject()
    {
        currentObject.useGravity = true;
        currentObject = null;
    }

    void MoveObject()
    {
        Vector3 directionToPoint = pickupPoint.position - currentObject.position;
        float distanceToPoint = directionToPoint.magnitude;

        currentObject.velocity = directionToPoint.normalized * 12f * distanceToPoint;
    }
}
