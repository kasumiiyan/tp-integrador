using UnityEngine;

public class Shield : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        // Verifica si el objeto que choc� est� en la capa de "Proyectil"
        if (other.gameObject.layer == LayerMask.NameToLayer("Proyectil"))
        {
           
            Destroy(other.gameObject);
        }
        else
        {
         
        }
    }
}
