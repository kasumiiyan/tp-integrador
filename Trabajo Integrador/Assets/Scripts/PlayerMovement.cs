using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public int maxHitPoints = 3;  // N�mero m�ximo de golpes que puede recibir el jugador
    public int currentHitPoints;

    public GameObject shieldPrefab;
    private GameObject shield;
    public float shieldDistance = 2.0f;

    public Text gameOverText;
    public float playerHP = 100;
    public float speed;
    public float jump;
    public float fallMultiplier = 2.5f;
    public PlayerCam playerCam;
    Rigidbody rb;
    public Transform GroundCheck;
    public LayerMask Ground;
    public Transform puntodeDisparo;

    bool isJumping = false;

    public Text healthText;

    // Prefab del proyectil
    public GameObject projectilePrefab;
    public float projectileSpeed = 10.0f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        currentHitPoints = maxHitPoints;

    }

    void Update()
    {


        if (currentHitPoints > 0)
        {
            float horizontal = Input.GetAxisRaw("Horizontal");
            float vertical = Input.GetAxisRaw("Vertical");

            Vector3 inputDir = playerCam.transform.forward * vertical + playerCam.transform.right * horizontal;
            inputDir.Normalize();

            Vector3 velocity = new Vector3(inputDir.x * speed, rb.velocity.y, inputDir.z * speed);
            rb.velocity = velocity;

            if (Input.GetButtonDown("Jump") && IsGrounded())
            {
                Jump();
            }

            if (Input.GetKeyDown(KeyCode.Q))
            {
                ToggleShield();  // Activa o desactiva el escudo al presionar Q
            }

            if (healthText != null)
            {
                healthText.text = "Vidas: " + currentHitPoints.ToString();
            }

            // Agrega la l�gica de disparo
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                ShootProjectile();
            }

            RotatePlayerTowardsMouse();
            UpdateShieldPositionAndRotation();

        }

    }

    void FixedUpdate()
    {
        if (currentHitPoints > 0)
        {
            ApplyManualGravity();
        }
    }

    void Jump()
    {
        if (currentHitPoints > 0)
        {
            rb.velocity = new Vector3(rb.velocity.x, jump, rb.velocity.z);
            isJumping = true;
        }
    }

    void ApplyManualGravity()
    {
        if (!IsGrounded() && !isJumping)
        {
            rb.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
    }

    bool IsGrounded()
    {
        return Physics.CheckSphere(GroundCheck.position, 0.1f, Ground);
    }

    void RotatePlayerTowardsMouse()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
        float rayDistance;

        if (groundPlane.Raycast(ray, out rayDistance))
        {
            Vector3 point = ray.GetPoint(rayDistance);
            point.y = transform.position.y;

            Quaternion targetRotation = Quaternion.LookRotation(point - transform.position);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * 10.0f);
        }
    }

    void ToggleShield()
    {
        if (shield == null)
        {
            // Crea el escudo si a�n no existe
            shield = Instantiate(shieldPrefab, transform.position + transform.forward * 2.0f, transform.rotation);
        }
        else
        {
            // Destruye el escudo si ya existe
            Destroy(shield);
            shield = null;
        }
    }

    Vector3 CalculateShieldPosition()
    {
        // Calcula la posici�n del escudo a una distancia del jugador y en la direcci�n en que est� mirando
        return transform.position + transform.forward * shieldDistance;
    }

    Quaternion CalculateShieldRotation()
    {
        // Calcula la rotaci�n del escudo para que est� rotado 90 grados respecto al jugador
        return Quaternion.Euler(0, transform.rotation.eulerAngles.y + 90.0f, 0);
    }

    void UpdateShieldPositionAndRotation()
    {
        // Actualiza la posici�n y rotaci�n del escudo para seguir al jugador
        if (shield != null)
        {
            shield.transform.position = CalculateShieldPosition();
            shield.transform.rotation = CalculateShieldRotation();
        }
    }

    void ShootProjectile()
    {
        // Instancia el proyectil en la posici�n del jugador
        GameObject newProjectile = Instantiate(projectilePrefab, puntodeDisparo.position, puntodeDisparo.rotation);

        // Obtiene el componente Rigidbody del proyectil
        Rigidbody projectileRb = newProjectile.GetComponent<Rigidbody>();

        // Aplica fuerza al proyectil en la direcci�n hacia adelante de la c�mara
        projectileRb.AddForce(playerCam.transform.forward * projectileSpeed, ForceMode.Impulse);
    }


    public void TakeDamage(float damage)
    {
        if (currentHitPoints > 0)  // Solo aplica el da�o si el jugador est� vivo
        {
            currentHitPoints -= 1;  // Reduce los puntos de golpe
           

            if (currentHitPoints <= 0)
            {
                GameOver();
            }
        }
    }

    // Nuevo m�todo para manejar el Game Over
    void GameOver()
    {
        
        Time.timeScale = 0;

       
        if (gameOverText != null)
        {
            gameOverText.text = "Game Over";
        }
        else
        {
            Debug.LogWarning("Game Over Text no asignado en el Inspector.");
        }
    }

    // M�todo para manejar las colisiones
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Proyectil"))
        {
            // Ajustar la cantidad de da�o
            float damage = 100;
            TakeDamage(damage);

            // Destruye el proyectil despu�s de colisionar
            Destroy(collision.gameObject);
        }
    }
}
